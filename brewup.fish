function brewup -d "upgrade and cleanup brew and brew casks"
	brew update;
	brew upgrade;
	brew upgrade --cask;
	brew cleanup;
	mas upgrade;
end