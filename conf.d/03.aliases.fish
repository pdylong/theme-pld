## ~/.config/fish/03.aliases.fish

set -g fish_user_paths "/usr/local/sbin" $fish_user_paths
#set -g fish_user_paths "/usr/local/opt/ncurses/bin" $fish_user_paths
#set -g fish_user_paths "/usr/local/opt/bench/bin" $fish_user_paths
set -g fish_user_paths "/bin" $fish_user_paths
set -g fish_user_paths "/Users/pld/bin" $fish_user_paths
set -g fish_user_paths "/usr/bin" $fish_user_paths
set -g fish_user_paths "/sbin" $fish_user_paths

# set the java8 alias depending on the mac I am on
if  string match -qr "(RELEASE_ARM64)" (uname -v)
    alias java8="/Library/Java/JavaVirtualMachines/zulu-jdk-8-macos_aarch64/bin/java"
else if string match -qr "(RELEASE_X86_64)" (uname -v)
    alias java8="/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/bin/java"
else
    echo "Java 8 alias could not be set, OS version could not be detected!"    
end
